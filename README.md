# MySQL

// Вывод всех Товаров 'X-BAG X-TRAVEL'
SELECT p.id_product
	, pl.name
	, REPLACE(
		REPLACE(
			pl.name, "Bric's", CONCAT(IF(LOCATE('BXG', pl.name) > 0, 'X-Bag', 'X-Travel'), " Bric's")
		)
	, '  ', ' ') as replace_name
     	 FROM ps_product AS p

	 INNER JOIN ps_category_lang as collection_cat
		ON collection_cat.name = 'X-BAG X-TRAVEL'

         INNER JOIN ps_category_product AS cp_collection 
		ON cp_collection.id_product = p.id_product 
		AND cp_collection.id_category = collection_cat.id_category


         INNER JOIN ps_product_lang as pl 
		ON p.id_product = pl.id_product

GROUP BY p.id_product
	, pl.name


// Обновление всех Товаров 'X-BAG X-TRAVEL'
UPDATE ps_product_lang as pl
	INNER JOIN ps_product AS p
		ON p.id_product = pl.id_product

 	 INNER JOIN ps_category_lang as collection_cat
		ON collection_cat.name = 'X-BAG X-TRAVEL'

        INNER JOIN ps_category_product AS cp_collection 
		ON cp_collection.id_product = p.id_product 
		AND cp_collection.id_category = collection_cat.id_category

SET pl.name = REPLACE(
		REPLACE(
			pl.name, "Bric's", CONCAT(IF(LOCATE('BXG', pl.name) > 0, 'X-Bag', 'X-Travel'), " Bric's")
		)
	, '  ', ' ')
	
	
	--------------------------------------------------------
	
// Вывод всех товаров кроме 'X-BAG X-TRAVEL'
SELECT p.id_product
	, pl.name
     	, sub_coll_name.name
	, REPLACE(REPLACE(pl.name, "Bric's", CONCAT(sub_coll_name.name, " Bric's")), '  ', ' ') as replace_name
     	 FROM ps_product AS p
	 INNER JOIN ps_category_lang as exclude_cat
		ON exclude_cat.name = 'X-BAG X-TRAVEL'

	 INNER JOIN ps_category_lang as collection_cat
		ON collection_cat.name = 'Коллекции'

         INNER JOIN ps_category_product AS cp_collection 
		ON cp_collection.id_product = p.id_product 
		AND cp_collection.id_category = collection_cat.id_category

         LEFT JOIN ps_category_product AS cp_xbag
		ON cp_xbag.id_product = p.id_product 
		AND cp_xbag.id_category = exclude_cat.id_category

         INNER JOIN ps_product_lang as pl 
		ON p.id_product = pl.id_product

	 INNER JOIN ps_category AS c
		ON c.id_parent = collection_cat.id_category

	INNER JOIN ps_category_product AS sub_coll_product
		ON sub_coll_product.id_category = c.id_category
        AND p.id_product = sub_coll_product.id_product 

	INNER JOIN ps_category_lang AS sub_coll_name
		ON sub_coll_name.id_category = sub_coll_product.id_category 
        
WHERE cp_xbag.id_product IS NULL
GROUP BY p.id_product
	, pl.name
        , sub_coll_name.name
HAVING LOCATE(sub_coll_name.name, pl.name) = 0


// Обновление всех товаров кроме 'X-BAG X-TRAVEL'
UPDATE ps_product_lang as pl
	INNER JOIN ps_product AS p
		ON p.id_product = pl.id_product

 	INNER JOIN ps_category_lang as exclude_cat
		ON exclude_cat.name = 'X-BAG X-TRAVEL'

	INNER JOIN ps_category_lang as collection_cat
		ON collection_cat.name = 'Коллекции'

        INNER JOIN ps_category_product AS cp_collection 
		ON cp_collection.id_product = p.id_product 
		AND cp_collection.id_category = collection_cat.id_category

        LEFT JOIN ps_category_product AS cp_xbag
		ON cp_xbag.id_product = p.id_product 
		AND cp_xbag.id_category = exclude_cat.id_category

	INNER JOIN ps_category AS c
		ON c.id_parent = collection_cat.id_category

	INNER JOIN ps_category_product AS sub_coll_product
		ON sub_coll_product.id_category = c.id_category
        	AND p.id_product = sub_coll_product.id_product 

	INNER JOIN ps_category_lang AS sub_coll_name
		ON sub_coll_name.id_category = sub_coll_product.id_category 

SET pl.name = REPLACE(REPLACE(pl.name, "Bric's", CONCAT(sub_coll_name.name, " Bric's")), '  ', ' ')
WHERE cp_xbag.id_product IS NULL

// Обновление всех товаров кроме 'X-BAG X-TRAVEL'
UPDATE ps_product_lang as pl
	INNER JOIN ps_product AS p
		ON p.id_product = pl.id_product

 	INNER JOIN ps_category_lang as exclude_cat
		ON exclude_cat.name = 'X-BAG X-TRAVEL'

	INNER JOIN ps_category_lang as collection_cat
		ON collection_cat.name = 'Коллекции'

        INNER JOIN ps_category_product AS cp_collection 
		ON cp_collection.id_product = p.id_product 
		AND cp_collection.id_category = collection_cat.id_category

        LEFT JOIN ps_category_product AS cp_xbag
		ON cp_xbag.id_product = p.id_product 
		AND cp_xbag.id_category = exclude_cat.id_category

	INNER JOIN ps_category AS c
		ON c.id_parent = collection_cat.id_category

	INNER JOIN ps_category_product AS sub_coll_product
		ON sub_coll_product.id_category = c.id_category
        	AND p.id_product = sub_coll_product.id_product 

	INNER JOIN ps_category_lang AS sub_coll_name
		ON sub_coll_name.id_category = sub_coll_product.id_category 

SET pl.name = REPLACE(REPLACE(pl.name, "Bric's", CONCAT(sub_coll_name.name, " Bric's")), '  ', ' ')
WHERE cp_xbag.id_product IS NULL



-------------------------------------------------------------

--- DELETE Invoices ---
SELECT 
GROUP_CONCAT(
    g.order_id SEPARATOR ','
)
FROM sales_invoice_grid AS g
LEFT JOIN sales_order AS o
    ON o.entity_id = g.order_id
WHERE o.entity_id IS NULL
into @deleteIds;

DELETE
FROM sales_invoice_grid
WHERE FIND_IN_SET(sales_invoice_grid.order_id,@deleteIds);


--- DELETE Shipments ---
SELECT 
GROUP_CONCAT(
    g.order_id SEPARATOR ','
)
FROM sales_shipment_grid AS g
LEFT JOIN sales_order AS o
    ON o.entity_id = g.order_id
WHERE o.entity_id IS NULL
into @deleteIds;

DELETE
FROM sales_shipment_grid
WHERE FIND_IN_SET(sales_shipment_grid.order_id,@deleteIds);


--- DELETE Credit Memos ---
SELECT 
GROUP_CONCAT(
    g.order_id SEPARATOR ','
)
FROM sales_creditmemo_grid AS g
LEFT JOIN sales_order AS o
    ON o.entity_id = g.order_id
WHERE o.entity_id IS NULL
into @deleteIds;

DELETE
FROM sales_creditmemo_grid
WHERE FIND_IN_SET(sales_creditmemo_grid.order_id,@deleteIds);